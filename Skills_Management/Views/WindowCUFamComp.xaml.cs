﻿using DAL;
using Microsoft.Practices.Prism.Mvvm;

using Skills_Management.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Skills_Management.Views
{
    /// <summary>
    /// Interaction logic for WindowCU_Fam_Comp.xaml
    /// </summary>
    public partial class WindowCUFamComp : UserControl//, IView
    {
        //public WindowCU_Fam_CompViewModel familleCompViewModel;

        public WindowCUFamComp()
        {
            InitializeComponent();
            //App.Eventaggr.GetEvent<WindowCUFamCompEvent>().Subscribe(LoadFamComp);
        }
    }
}
