﻿using DAL;
using Skills_Management.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Collections;

using Prism.Interactivity.InteractionRequest;
using Microsoft.Practices.Prism.Commands;
using Skills_Management.Notifications;
using Microsoft.Practices.Prism.Mvvm;

namespace Skills_Management.ViewModels
{
    //public class WindowCUFamCompViewModel : INotifyPropertyChanged, INotifyDataErrorInfo, IInteractionRequestAware
    public class WindowCUFamCompViewModel : BindableBase, INotifyDataErrorInfo, IInteractionRequestAware
    {
        public WindowCUFamCompViewModel()
        {
            this.ValidateCommand = new DelegateCommand(this.ValidateItem);
            this.CloseCommand = new DelegateCommand(this.CloseWindow);
        }

        #region DelegateCommands

        public DelegateCommand ValidateCommand { get; private set; }
        public DelegateCommand CloseCommand { get; set; }

        // 08/07/2016
        private void ValidateItem()
        {
            using (var ctx = new BASE_COMPETENCESEntities())
            {
                this.Validate();
                //Retour si erreur
                if (this.HasErrors) return;
                int? IdFamComp = this.FamCompId;
                // Si nous sommes en mode création
                if (IdFamComp == 0)
                {
                    TFamComp tfcomp = new TFamComp();
                    tfcomp.FAM_COMP_NAME = this.FamCompNom;
                    ctx.TFamComps.Add(tfcomp);
                }
                else
                {
                    TFamComp fcomp = ctx.TFamComps.Where(t => t.FAM_COMP_ID == IdFamComp).FirstOrDefault();
                    fcomp.FAM_COMP_NAME = this.FamCompNom;
                }

                ctx.SaveChanges();
                this.FinishInteraction();
            }
        }

        private void CloseWindow()
        {
            this.FinishInteraction();
        }

        #endregion

        #region Properties

        private string _FamCompNom = string.Empty;
        public string FamCompNom
        {
            get
            {
                return _FamCompNom;
            }

            set
            {
                _FamCompNom = value;
                this.OnPropertyChanged(() => this.FamCompNom);
            }
        }

        private int _FamCompId;
        public int FamCompId
        {
            get
            {
                return _FamCompId;
            }

            set
            {
                _FamCompId = value;
                this.OnPropertyChanged(() => this.FamCompId);
            }
        }

        #endregion

        #region INotifyDataErrorInfo

        Dictionary<string, List<string>> propErrors = new Dictionary<string, List<string>>();

        private void Validate()
        {
            //Task.Run(() => DataValidation());
            DataValidation();
        }

        private void DataValidation()
        {
            //Validate Name property
            List<string> listErrors;
            /*
            if (propErrors.TryGetValue(FamCompNom, out listErrors) == false)
                listErrors = new List<string>();
            else
                listErrors.Clear();
            */
            listErrors = new List<string>();
            if (string.IsNullOrEmpty(FamCompNom))
                listErrors.Add("Name should not be empty!!!");

            // 30/06/2016 : tester si des doublons sont présents
            using (var ctx = new BASE_COMPETENCESEntities())
            {
                bool bFlagExist = false;
                foreach (var famcomp in ctx.TFamComps)
                {
                    bool bCondNOK = (famcomp.FAM_COMP_NAME.ToUpper() == FamCompNom.ToUpper());
                    // En mode Modification
                    if (FamCompId != 0)
                        bCondNOK = bCondNOK & (famcomp.FAM_COMP_ID != FamCompId);
                    if (bCondNOK)
                    {
                        bFlagExist = true;
                        break;
                    }
                }
                if (bFlagExist)
                    listErrors.Add("Déjà une famille de compétences nommée " + FamCompNom);
            }

            propErrors["FamCompNom"] = listErrors;

            //if (listErrors.Count > 0)
            //{
            OnPropertyErrorsChanged("FamCompNom");
            //}
        }

        private void OnPropertyErrorsChanged(string p)
        {
            if (ErrorsChanged != null)
                ErrorsChanged.Invoke(this, new DataErrorsChangedEventArgs(p));
        }

        public bool HasErrors
        {
            get
            {
                //throw new NotImplementedException();
                try
                {
                    var propErrorsCount = propErrors.Values.FirstOrDefault(r => r.Count > 0);
                    if (propErrorsCount != null)
                        return true;
                    else
                        return false;
                }
                catch { }
                return true;
            }
        }

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        public IEnumerable GetErrors(string propertyName)
        {
            List<string> errors = new List<string>();
            if (propertyName != null)
            {
                propErrors.TryGetValue(propertyName, out errors);
                return errors;
            }
            else
                return null;

        }

        #endregion

        #region IInteractionRequestAware
        private CUFamCompNotification notification;
        public INotification Notification
        {
            get
            {
                return this.notification;
            }
            set
            {
                if (value is CUFamCompNotification)
                {
                    this.notification = value as CUFamCompNotification;
                    this.OnPropertyChanged(() => this.Notification);
                    // If we are in creation mode
                    if (this.notification.famComp == null)
                    {
                        this.FamCompNom = string.Empty;
                        this.FamCompId = 0;
                    }
                    else
                    {
                        TFamComp famComp = (TFamComp)((CUFamCompNotification)this.Notification).famComp;
                        this.FamCompId = famComp.FAM_COMP_ID;
                        this.FamCompNom = famComp.FAM_COMP_NAME;
                    }
                    this.OnPropertyChanged(() => this.FamCompNom);
                    this.OnPropertyChanged(() => this.FamCompId);
                }
            }
        }

        public Action FinishInteraction { get; set; }

        #endregion
                
    }
}
