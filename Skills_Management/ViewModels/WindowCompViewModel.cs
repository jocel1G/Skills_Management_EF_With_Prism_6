﻿using DAL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Windows.Input;
using Skills_Management.Views;
using System.Windows;

using Microsoft.Practices.Prism.Mvvm;
// 18/07/2016 : en commentaire, au profit de la ligne suivante, et ceci pour avoir la méthode ObservesProperty
// pour la classe DelegateCommand.
//using Microsoft.Practices.Prism.Commands;
using Prism.Commands;
using Prism.Regions;
using Infrastruct;
using CompModule.Views;
using GlobalResources;

// Page http://www.help-doing.com/sm/wpf-using-inotifydataerrorinfoinotifypropertychanged-force-validation-on-all-input-fields-on-save.shtml
// pour l'interface INotifyDataErrorInfo

namespace Skills_Management.ViewModels
{
    //public class WindowCompViewModel : INotifyPropertyChanged, INotifyDataErrorInfo
    public class WindowCompViewModel : BindableBase, INotifyDataErrorInfo
    {
        private readonly IRegionManager _regionManager;

        private bool fbCanExecuteUpdate_AndDelete()
        {
            return this.SelectedItem != null;
        }

        public bool fbCanExecuteAdd()
        {
            return !fbCanExecuteUpdate_AndDelete();
        }

        public WindowCompViewModel(IRegionManager regionManager): this()
        {
            _regionManager = regionManager;

            
        }

        public WindowCompViewModel()
        {
            // Lignes pour On Demand. Sinon, mettre en commentaire, et modifier dans la méthode ConfigureModuleCatalog() de la classe Bootstrapper.
            // 19/07/2016 : ligne ServiceLocator.Current.GetInstance...
            // trouvée à la page http://www.infragistics.com/community/blogs/blagunas/archive/2013/08/06/prism-dynamically-discover-and-load-modules-at-runtime.aspx
            Prism.Modularity.IModuleManager manager = Microsoft.Practices.ServiceLocation.ServiceLocator.Current.GetInstance<Prism.Modularity.IModuleManager>();
            Type CUFamCompModuleType = typeof(CompModule.CompModule);
            manager.LoadModule(CUFamCompModuleType.Name);

            using (var ctx = new BASE_COMPETENCESEntities())
            {
                TCompsLoad_Et_Local(ctx);
            }

            //08/07/2016
            #region DelegateCommands
            // 18/07/2016 : les trois lignes ci-dessous sont modifiées avec l'ajout de ObservesProperty.
            this.AddCompCommandDC = new DelegateCommand(this.ActAddComp, fbCanExecuteAdd).ObservesProperty(() => SelectedItem);
            UpdateCompCommandDC = new DelegateCommand(ActUpdateComp, fbCanExecuteUpdate_AndDelete).ObservesProperty(() => SelectedItem) ;
            DeleteCompCommandDC = new DelegateCommand(ActDeleteComp, fbCanExecuteUpdate_AndDelete).ObservesProperty(() => SelectedItem);
            RetModeCreaCommandDC = new DelegateCommand(RetModeAddComp);
            #endregion
        }

        public void TCompsLoad_Et_Local(BASE_COMPETENCESEntities ctx)
        {
            ctx.TComps.Load();
            ListComp = ctx.TComps.Local;
        }

        #region Properties

        // List <-> ObservableCollection
        private ObservableCollection<TComp> _ListComp;
        public ObservableCollection<TComp> ListComp
        {
            get
            {
                return _ListComp;
            }
            set
            {
                _ListComp = value;
                //OnPropertyChanged("ListComp");
                this.OnPropertyChanged(() => this.ListComp);
            }
        }

        private TComp _SelectedItem;
        public TComp SelectedItem
        {
            get
            {
                return _SelectedItem;
            }
            set
            {
                _SelectedItem = value;
                //OnPropertyChanged("SelectedItem");
                this.OnPropertyChanged(() => this.SelectedItem);
                ERRORS = string.Empty;

                ClearErrors();

                //22/07/2016 : remove Region
                string strViewTitle = (_SelectedItem != null) ? ClassGlob.RTFN_UCButtonCompAdd : ClassGlob.RTFN_UCButtonCompUDRMC;
                try
                {
                    var viewToRemove = _regionManager.Regions[RegionNames.ButtonCompRegion].Views.FirstOrDefault<dynamic>(v => v.ViewTitle == strViewTitle);
                    _regionManager.Regions[RegionNames.ButtonCompRegion].Remove(viewToRemove);
                }
                catch (Exception ex)
                {

                }

                // 11/07/2016 : Mise à jour des champs ID, Nom et Famille de la compétence.
                if (_SelectedItem != null)
                {
                    COMP_ID = _SelectedItem.COMP_ID;
                    COMP_NAME = _SelectedItem.COMP_NAME;
                    FK_FAM_COMP_ID = _SelectedItem.FK_FAM_COMP_ID;

                    _regionManager.RequestNavigate(RegionNames.ButtonCompRegion, ClassGlob.RTFN_UCButtonCompUDRMC);
                }
                else
                {
                    COMP_ID = 0;
                    COMP_NAME = string.Empty;
                    FK_FAM_COMP_ID = -1;

                    _regionManager.RequestNavigate(RegionNames.ButtonCompRegion, ClassGlob.RTFN_UCButtonCompAdd);
                }
                // 18/07/2016 : trois lignes ci-dessous inutiles
                //AddCompCommandDC.RaiseCanExecuteChanged();
                //UpdateCompCommandDC.RaiseCanExecuteChanged();
                //DeleteCompCommandDC.RaiseCanExecuteChanged();


            }
        }

        // 11/07/2016 : ID of the comp
        private int _COMP_ID;
        public int COMP_ID
        {
            get
            {
                return _COMP_ID;
            }
            set
            {
                _COMP_ID = value;
                OnPropertyChanged(() => COMP_ID);
            }
        }

        //Name of the comp
        private string _COMP_NAME;
        public string COMP_NAME
        {
            get
            {
                return _COMP_NAME;
            }
            set
            {
                _COMP_NAME = value;
                OnPropertyChanged(() => COMP_NAME);
            }
        }

        private int _FK_FAM_COMP_ID;
        public int FK_FAM_COMP_ID
        {
            get
            {
                return _FK_FAM_COMP_ID;
            }
            set
            {
                _FK_FAM_COMP_ID = value;
                OnPropertyChanged(() => FK_FAM_COMP_ID);
            }
        }

        #endregion

        #region INotifyDataErrorInfo

        private string _ERRORS;
        public string ERRORS
        {
            get
            {
                return _ERRORS;
            }
            set
            {
                _ERRORS = value;
                //OnPropertyChanged("ERRORS");
                this.OnPropertyChanged(() => this.ERRORS);
            }
        }

        private Dictionary<string, List<string>> propErrors = new Dictionary<string, List<string>>();

        private void Validate(bool bCrea = true)
        {
            //Task.Run(() => DataValidation());
            DataValidation(bCrea);
        }

        private void DataValidation(bool bCrea = true)
        {
            //Validate Name property
            List<string> listErrors;
            //if (propErrors.TryGetValue(propname, out listErrors) == false)
            //    listErrors = new List<string>();
            //else
            //    listErrors.Clear();
            listErrors = new List<string>();

            string strError = string.Empty;

            // Test sur le nom de la compétence
            if (String.IsNullOrEmpty(COMP_NAME) || String.IsNullOrWhiteSpace(COMP_NAME))
            {
                //listErrors.Add("Name is required");
                strError = "Name is required";
            }
            //Test sur la famille de compétences
            if (FK_FAM_COMP_ID <= 0)
            {
                //listErrors.Add("Skills family is required");
                strError += (string.IsNullOrEmpty(strError) ? String.Empty : "\n") + "Skills family is required";
            }

            // 05/07/2016 : test sur les doublons
            using (var ctx = new BASE_COMPETENCESEntities())
            {
                bool bExist = false;
                foreach (var comp in ctx.TComps)
                {
                    bool bTest = COMP_NAME.ToUpper() == comp.COMP_NAME.ToUpper();
                    // If we are not in creation mode
                    if (!bCrea)
                        bTest = bTest && (SelectedItem.COMP_ID != comp.COMP_ID);
                    if (bTest)
                    {
                        bExist = true;
                        break;
                    }
                }

                if (bExist)
                    strError += (string.IsNullOrEmpty(strError) ? String.Empty : "\n") + "La compétence " + COMP_NAME + " existe déjà";
            }

            if (!string.IsNullOrEmpty(strError))
                listErrors.Add(strError);

            propErrors["ERRORS"] = listErrors;

            //if (listErrors.Count > 0)
            //{
            ERRORS = strError;
            OnPropertyErrorsChanged("ERRORS");

            //}

        }

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        private void OnPropertyErrorsChanged(string p)
        {
            if (ErrorsChanged != null)
                ErrorsChanged.Invoke(this, new DataErrorsChangedEventArgs(p));
        }

        public System.Collections.IEnumerable GetErrors(string propertyName)
        {
            List<string> errors = new List<string>();
            if (propertyName != null)
            {
                propErrors.TryGetValue(propertyName, out errors);
                return errors;
            }

            else
                return null;
        }

        public bool HasErrors
        {
            get
            {
                try
                {
                    var propErrorsCount = propErrors.Values.FirstOrDefault(r => r.Count > 0);
                    if (propErrorsCount != null)
                        return true;
                    else
                        return false;
                }
                catch { }
                return true;
            }
        }

        private void ClearErrors()
        {
            this.propErrors.Clear();
            this.OnPropertyErrorsChanged("ERRORS");
        }

        #endregion

        #region DelegateCommand

        #region Add Skill
        public DelegateCommand AddCompCommandDC { get; private set; }        

        private void ActAddComp()
        {
            this.Validate();

            if (this.HasErrors) return;

            //int intFamCompId = (int)winComp.cboFamComp.SelectedValue;
            using (var ctx = new BASE_COMPETENCESEntities())
            {
                TComp tcomp = new TComp() { COMP_NAME = COMP_NAME, FK_FAM_COMP_ID = FK_FAM_COMP_ID };
                ctx.TComps.Add(tcomp);
                ctx.SaveChanges();
                //WindowCompViewModel compViewModel = (WindowCompViewModel)winComp.grdComp.DataContext;
                TCompsLoad_Et_Local(ctx);
                // Sélectionner la compétence qui vient d'être créée, ceci afin de disposer de son identifiant,
                // au cas où une modification serait opérée par la suite sur ce même enregistrement.

                // En fait, rien n'a pas dû être fait ici. J'ai repris la page http://www.entityframeworktutorial.net/EntityFramework5/CRUD-using-stored-procedures.aspx,
                // en modifiant la procédure stockée sp_InsertComp -> SELECT SCOPE_IDENTITY() AS CompId;
                // au lieu de RETURN SCOPE_IDENTITY()
                // Et modification du EDMX.
            }

            
        }
        #endregion

        #region Update Skill
        public DelegateCommand UpdateCompCommandDC { get; private set; }

        private void ActUpdateComp()
        {
            this.Validate(false);

            if (this.HasErrors) return;

            int intFamCompId = (int)this.FK_FAM_COMP_ID;
            using (var ctx = new BASE_COMPETENCESEntities())
            {
                
                TComp tcomp = ctx.TComps.Where(tc => tc.COMP_ID == this.SelectedItem.COMP_ID).FirstOrDefault();
                tcomp.COMP_NAME = COMP_NAME;
                tcomp.FK_FAM_COMP_ID = FK_FAM_COMP_ID;
                ctx.SaveChanges();

                TCompsLoad_Et_Local(ctx);
            }
        }
        #endregion

        #region Delete Skill
        public DelegateCommand DeleteCompCommandDC { get; private set; }

        private void ActDeleteComp()
        {            
            MessageBoxResult res = MessageBox.Show("Voulez-vous supprimer la compétence " + SelectedItem.COMP_NAME + " ?", "Suppression", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if (res == MessageBoxResult.OK)
            {
                int intFamCompId = (int)SelectedItem.COMP_ID;
                using (var ctx = new BASE_COMPETENCESEntities())
                {
                    TComp tcomp = ctx.TComps.Where(tc => tc.COMP_ID == intFamCompId).FirstOrDefault();
                    ctx.TComps.Remove(tcomp);
                    ctx.SaveChanges();

                    TCompsLoad_Et_Local(ctx);
                }
            }
        }
        #endregion

        #region Retour mode création
        public DelegateCommand RetModeCreaCommandDC { get; private set; }

        private void RetModeAddComp()
        {
            SelectedItem = null;
        }
        #endregion

        #endregion
    }
}
