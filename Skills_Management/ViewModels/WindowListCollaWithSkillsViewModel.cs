﻿using DAL;
using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Windows.Data;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Microsoft.Practices.Prism.Commands;


namespace Skills_Management.ViewModels
{
    public class WindowListCollaWithSkillsViewModel: BindableBase
    {
        #region Constructor
        public WindowListCollaWithSkillsViewModel()
        {            
            using (var ctx = new BASE_COMPETENCESEntities())
            {
                ctx.TComps.Load();
                ListComp = ctx.TComps.Local ;
                foreach (TComp comp in ListComp)
                {
                    TCompWithBoolAndLevel tco = new TCompWithBoolAndLevel();
                    tco.COMP_ID = comp.COMP_ID;
                    tco.COMP_NAME = comp.COMP_NAME;
                    tco.FK_FAM_COMP_ID = comp.FK_FAM_COMP_ID;
                    ListCompBis.Add(tco);
                }
            }
            RefreshListCollaManyCompCommandDC = new DelegateCommand(RefreshListCollaManyComp);
            
        }

        private void RefreshListCollaManyComp()
        {
            using (var ctx = new BASE_COMPETENCESEntities())
            {
                // Phase #1: for each skill, record the list of employees with the skill, taking into account the level, if selected.
                //ListCollaManyComp = new ListCollectionView(ctx.sp_GET_COLLA_WITH_COMP(null, SelectedItemComp.COMP_ID).ToArray());
                Dictionary<int, ObservableCollection<sp_GET_COLLA_WITH_COMP_Result>> dict = new Dictionary<int, ObservableCollection<sp_GET_COLLA_WITH_COMP_Result>>();
                for (int i = 0; i < ListCompBis.Count; i++)
                {
                    //Console.WriteLine(ListCompBis[i].COMP_Checked.ToString());
                    //Console.WriteLine(ListCompBis[i].COMP_ID.ToString());
                    //Console.WriteLine(ListCompBis[i].Level_Collab.ToString());
                    if (ListCompBis[i].COMP_Checked)
                    {
                        Predicate<sp_GET_COLLA_WITH_COMP_Result> pred = delegate(sp_GET_COLLA_WITH_COMP_Result sp)
                         {
                             // Si un niveau a été sélectionné
                             if (ListCompBis[i].Level_Collab > 0)
                                 return sp.LEVEL_COLLA >= ListCompBis[i].Level_Collab;
                             else
                                 return true;
                         };

                        dict.Add(ListCompBis[i].COMP_ID, new ObservableCollection<sp_GET_COLLA_WITH_COMP_Result>(ctx.sp_GET_COLLA_WITH_COMP(null, ListCompBis[i].COMP_ID).Where(s => pred(s)).ToArray()));
                    }
                }
                //ListCollaManyComp = new ObservableCollection<sp_GET_COLLA_WITH_COMP_Result>(ctx.sp_GET_COLLA_WITH_COMP(null, 13).Where(s=>s.LEVEL_COLLA>=6).ToArray()) ;
                //ListCollaManyComp = new ObservableCollection<sp_GET_COLLA_WITH_COMP_Result>(ctx.sp_GET_COLLA_WITH_COMP(null, 13).Where(s => s.LEVEL_COLLA >= 6).ToArray());
                //ListCollaManyComp = new ObservableCollection<sp_GET_COLLA_WITH_COMP_Result>(ctx.sp_GET_COLLA_WITH_COMP(null, 13).ToArray());
                //ListCollaManyComp

                ListCollaManyComp = new ObservableCollection<sp_GET_COLLA_WITH_COMP_Result>();
                bool bFirst = true;
                foreach (KeyValuePair<int, ObservableCollection<sp_GET_COLLA_WITH_COMP_Result>> kvp in dict)
                {
                    // Si c'est le premier élément du dictionnaire, la liste à afficher contiendra la liste des collaborateurs ayant la première compétence
                    if (bFirst)
                    {
                        ListCollaManyComp = kvp.Value;
                        bFirst = false;
                    }
                    else
                    {
                        // Test si chaque colla enregistré fait partie de ceux dans l'élément actuel du dictionnaire
                        int jj = 0;
                        while (jj < ListCollaManyComp.Count)
                        {                         
                            bool bFlagContainsColla = false;
                            for (int ii = 0; ii<kvp.Value.Count;ii++)
                            {
                                int intCollaId = kvp.Value[ii].COLLA_ID;
                            
                                if (ListCollaManyComp[jj].COLLA_ID == intCollaId)
                                {
                                    bFlagContainsColla = true;
                                    break;
                                }
                            }
                            if (!bFlagContainsColla)
                            {
                                ListCollaManyComp.RemoveAt(jj);
                                jj = 0;
                            }
                            else
                                jj++;
                        }
                    }
                }
            }
        }

        #endregion

        #region Properties

        #region Skills

        private ObservableCollection<TComp> _ListComp;
        public ObservableCollection<TComp> ListComp
        {
            get { return _ListComp; }
            set { SetProperty(ref _ListComp, value); }
        }

        private ObservableCollection<TCompWithBoolAndLevel> _ListCompBis = new ObservableCollection<TCompWithBoolAndLevel>();
        public ObservableCollection<TCompWithBoolAndLevel> ListCompBis
        {
            get { return _ListCompBis; }
            set { SetProperty(ref _ListCompBis, value); }
        }

        private TComp _SelectedItemComp;
        public TComp SelectedItemComp
        {
            get { return _SelectedItemComp; }
            set
            {
                SetProperty(ref _SelectedItemComp, value);
                UpdateListCollaOneComp();
            }
        }

        private ObservableCollection<byte> _ListLevels;
        public ObservableCollection<byte> ListLevels
        {
            get { return _ListLevels; }
            set { SetProperty(ref _ListLevels, value); }
        }

        #endregion

        #region Colla

        //private string _CollaName;
        //public string CollaName
        //{
        //    get { return _CollaName; }
        //    set { SetProperty(ref _CollaName, value); }
        //}

        //private string _CollaFirstName;
        //public string CollaFirstName
        //{
        //    get { return _CollaFirstName; }
        //    set { SetProperty(ref _CollaFirstName, value); }
        //}

        private ICollectionView _ListCollaOneComp;
        public ICollectionView ListCollaOneComp
        {
            get { return _ListCollaOneComp; }
            set { SetProperty(ref _ListCollaOneComp, value); }
        }

        private ObservableCollection<sp_GET_COLLA_WITH_COMP_Result> _ListCollaManyComp;
        public ObservableCollection<sp_GET_COLLA_WITH_COMP_Result> ListCollaManyComp
        {
            get { return _ListCollaManyComp; }
            set { SetProperty(ref _ListCollaManyComp, value); }
        }

        #endregion
        #endregion

        private void UpdateListCollaOneComp()
        {
            using (var ctx = new BASE_COMPETENCESEntities())
            {
                ListCollaOneComp =new ListCollectionView(ctx.sp_GET_COLLA_WITH_COMP(null, SelectedItemComp.COMP_ID).ToArray());
            }
        }

        #region DelegateCommand
        public DelegateCommand RefreshListCollaManyCompCommandDC { get; private set; }
        #endregion
    }
}
