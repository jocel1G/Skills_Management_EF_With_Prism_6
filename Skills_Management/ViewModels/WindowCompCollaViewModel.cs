﻿using CollaModule.Colla;
using CollaModule.Colla.Interfaces;
using CollaModule.Comp_Colla;
using DAL;
using Events;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.ServiceLocation;
using Prism.Events;
using Prism.Modularity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Skills_Management.ViewModels
{
    public class WindowCompCollaViewModel : BindableBase
    {
        private readonly IEventAggregator eventAggregator;
        private readonly CompCollaDataService cCompCollaDataService;

        #region Properties

        #region Colla
        // Servira après
        //private int _COLLA_ID;
        //public int COLLA_ID
        //{
        //    get { return _COLLA_ID; }
        //    set { SetProperty(ref _COLLA_ID, value); }
        //}

        //private string _COLLA_NAME;
        //public string COLLA_NAME
        //{
        //    get { return _COLLA_NAME; }
        //    set { SetProperty(ref _COLLA_NAME, value); }
        //}

        private ICollectionView _ListColla;
        public ICollectionView ListColla
        {
            get { return _ListColla; }
            set { SetProperty(ref _ListColla, value); }
        }

        private TColla _SelectedItemColla;
        public TColla SelectedItemColla
        {
            get
            {
                return _SelectedItemColla;
            }
            set
            {
                SetProperty(ref _SelectedItemColla, value);
            }
        }

        #endregion

        #region Comp Colla
        // Inutile
        //////////private int _COMP_ID;
        //////////public int COMP_ID
        //////////{
        //////////    get { return _COMP_ID; }
        //////////    set { SetProperty(ref _COMP_ID, value); }
        //////////}

        //private string _COMP_NAME;
        //public string COMP_NAME
        //{
        //    get { return _COMP_NAME; }
        //    set { SetProperty(ref _COMP_NAME, value); }
        //}

        // 27/07/2016
        private int _FK_COMP_ID;
        public int FK_COMP_ID
        {
            get
            {
                return _FK_COMP_ID;
            }
            set
            {
                SetProperty(ref _FK_COMP_ID, value);
                
            }
        }

        //(1)
        //private ICollectionView _ListCompColla;
        //public ICollectionView ListCompColla
        //{
        //    get { return _ListCompColla; }
        //    set { SetProperty(ref _ListCompColla, value); }
        //}

        private ObservableCollection<sp_GET_COMP_OF_COLLA_Result> _ListCompColla;
        public ObservableCollection<sp_GET_COMP_OF_COLLA_Result> ListCompColla
        {
            get { return _ListCompColla; }
            set { SetProperty(ref _ListCompColla, value); }
        }

        private sp_GET_COMP_OF_COLLA_Result _SelectedItemCompColla;
        public sp_GET_COMP_OF_COLLA_Result SelectedItemCompColla
        {
            get
            {
                return _SelectedItemCompColla;
            }
            set
            {
                SetProperty(ref _SelectedItemCompColla, value);
                
                if (_SelectedItemCompColla != null)
                {
                    // 16/08/2016
                    if (_SelectedItemCompColla.FK_COMP_ID.HasValue)
                        FK_COMP_ID = _SelectedItemCompColla.FK_COMP_ID.Value;
                    else
                        FK_COMP_ID = 0;
                }
                
            }
        }

        private int _LEVEL_COLLA;
        public int LEVEL_COLLA
        {
            get { return _LEVEL_COLLA; }
            set { SetProperty(ref _LEVEL_COLLA, value); }
        }

        #endregion

        #endregion

        #region Constructor        

        public WindowCompCollaViewModel(CollaDataService cDataService, CompCollaDataService cCompCollaDataService, IEventAggregator eventAggregator)
        {
            // 13/07/2016 : ligne ServiceLocator.Current.GetInstance...
            // trouvée à la page http://www.infragistics.com/community/blogs/blagunas/archive/2013/08/06/prism-dynamically-discover-and-load-modules-at-runtime.aspx
            IModuleManager manager = ServiceLocator.Current.GetInstance<IModuleManager>();
            #region Colla
            Type TableCollaModuleType = typeof(TableCollaModule);
            manager.LoadModule(TableCollaModuleType.Name);

            // Either this
            _ListColla = System.Windows.Data.CollectionViewSource
                    .GetDefaultView(cDataService.getCollas());
            // Or that
            //_ListColla = cDataService.getCollas();

            this.ListColla.CurrentChanged += new EventHandler(this.SelectedEmployeeChanged);
            
            #endregion

            #region Comp colla
            Type TableComp_CollaModuleType = typeof(TableComp_CollaModule);
            manager.LoadModule(TableComp_CollaModuleType.Name);
            this.cCompCollaDataService = cCompCollaDataService;
            
            #endregion

            #region Event
            this.eventAggregator = eventAggregator;
            this.eventAggregator.GetEvent<EmployeeSelectedEvent>().Subscribe(this.EmployeeSelected, true);
            #endregion


            SaveCompCollaCommandDC = new DelegateCommand(this.SaveCompColla);
        }
        #endregion

        #region Events

        private void SelectedEmployeeChanged(object sender, EventArgs e)
        {
            TColla colla = this.ListColla.CurrentItem as TColla;
            if (colla != null)
            {
                // Publish the EmployeeSelectedEvent event.
                this.eventAggregator.GetEvent<EmployeeSelectedEvent>().Publish(colla);
            }
        }

        private void EmployeeSelected(TColla colla)
        {
            //Predicate<object> predCompColla = delegate (object obj)
            //{
            //    return ((TColla)obj).COLLA_ID == colla.COLLA_ID;
            //};

            //ListCompColla.Filter = predCompColla;
            //ListCompColla.Refresh();
            // Either this

            
            SelectedItemColla = ListColla.CurrentItem as TColla;

            ListCompColla = this.cCompCollaDataService.getCompCollas(colla.COLLA_ID);

            if (ListCompColla.Count != 0)
                SelectedItemCompColla = ListCompColla[0];

        }

        #endregion
        public DelegateCommand SaveCompCollaCommandDC { get; private set; }

    

        private void SaveCompColla()
        {
            if (ListCompColla.Count>0)
            {
                try
                {
                    int intLigCompDoublon_i = -1;
                    int intLigCompDoublon_j = -1;

                    for (int i = 0; i < ListCompColla.Count; i++)
                    {
                        if (ListCompColla[i].FK_COMP_ID==null)
                        {
                            MessageBox.Show("La ligne " + (i+1).ToString() + " est mal renseignée.");
                            return;
                        }
                    }

                    // Balayage des lignes dans la grille, pour la recherche de doublons
                    for (int i = 0; i < ListCompColla.Count - 1; i++)
                    {
                        for (int j = i + 1; j < ListCompColla.Count; j++)
                        {
                            if (ListCompColla[i].FK_COMP_ID == ListCompColla[j].FK_COMP_ID)
                            {
                                intLigCompDoublon_j = j;
                                break;
                            }
                        }
                        if (intLigCompDoublon_j != -1)
                        {
                            intLigCompDoublon_i = i;
                            break;
                        }
                    }
                    using (var ctx = new BASE_COMPETENCESEntities())
                    {
                        if (intLigCompDoublon_j != -1)
                        {
                            string strCompName = ((TComp)ctx.TComps.ToList().FirstOrDefault(s => s.COMP_ID == ListCompColla[intLigCompDoublon_j].FK_COMP_ID)).COMP_NAME;
                            MessageBox.Show("Compétence " + strCompName + ".\nDoublon sur les lignes " + (intLigCompDoublon_i + 1).ToString() + " et " + (intLigCompDoublon_j + 1).ToString() + ".", "Doublon compétence", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                            return;
                        }
                    }
                    // Suppression de toutes les compétences rattachées au colla
                    using (var ctx = new BASE_COMPETENCESEntities())
                    {
                        ctx.TCompCollas.RemoveRange(ctx.TCompCollas.Where(t => t.FK_COLLA_ID == SelectedItemColla.COLLA_ID));

                        ctx.SaveChanges();

                        // Création des nouveaux liens colla - compétences
                        for (int i = 0; i < ListCompColla.Count; i++)
                        {
                            TCompColla compColla = new TCompColla()
                            {
                                FK_COLLA_ID = SelectedItemColla.COLLA_ID,
                                LEVEL_COLLA = ListCompColla[i].LEVEL_COLLA,
                                // 16/08/2016
                                FK_COMP_ID = ListCompColla[i].FK_COMP_ID.Value
                            };
                            ctx.TCompCollas.Add(compColla);
                        }

                        ctx.SaveChanges();

                        EmployeeSelected(SelectedItemColla);

                        MessageBox.Show("Les associations ont bien été créées.", "Compétences du colla", MessageBoxButton.OK, MessageBoxImage.Information);

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("L'erreur suivante s'est produite : \n" + ex.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
    }

    
}
