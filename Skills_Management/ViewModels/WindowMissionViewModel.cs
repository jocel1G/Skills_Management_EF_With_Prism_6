﻿using DAL;
using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel;
using System.Windows.Data;
// 17/08/2016: as in the class WindowCompViewModel, to have ObservesProperty
//using Microsoft.Practices.Prism.Commands;
using Prism.Commands;
using System.Windows;
using Prism.Events;

namespace Skills_Management.ViewModels
{
    public class WindowMissionViewModel : BindableBase
    {
        #region Properties
        private ObservableCollection<TMission> _ListMission;
        //private ICollectionView _ListMission;

        public ObservableCollection<TMission> ListMission
        {
            get
            {
                return _ListMission;
            }
            set
            {
                SetProperty(ref _ListMission, value);
            }
        }

        private TMission _SelectedItem;
        public TMission SelectedItem
        {
            get
            {
                return _SelectedItem;
            }
            set
            {
                if (SetProperty(ref _SelectedItem, value))
                {
                    if (_SelectedItem!=null)
                    {
                        MISSION_ID_P = _SelectedItem.MISSION_ID;
                        MISSION_LABEL_P = _SelectedItem.MISSION_LABEL;
                        MISSION_DESC_P = _SelectedItem.MISSION_DESC;
                        ListOfSkills = new ObservableCollection<T_GET_COMP_OF_MISSIONWithChecked>();
                        using (var ctx = new BASE_COMPETENCESEntities())
                        {
                            LoadSkillsForMission(ctx);
                        }
                    }
                    else
                    {
                        MISSION_ID_P = 0;
                        MISSION_LABEL_P = string.Empty;
                        MISSION_DESC_P = string.Empty;
                    }
                    // 17/08/2016: Hide or show the tab dedicated to the required skills.
                    TabSkillsVisible = SelectedItem != null;
                }
            }
        }

        private int _MISSION_ID_P;
        public int MISSION_ID_P
        {
            get
            {
                return _MISSION_ID_P;
            }
            set
            {
                SetProperty(ref _MISSION_ID_P, value);
            }
        }

        private string _MISSION_LABEL_P;
        public string MISSION_LABEL_P
        {
            get
            {
                return _MISSION_LABEL_P;
            }
            set
            {
                SetProperty(ref _MISSION_LABEL_P, value);
            }
        }

        private string _MISSION_DESC_P;
        public string MISSION_DESC_P
        {
            get
            {
                return _MISSION_DESC_P;
            }
            set
            {
                SetProperty(ref _MISSION_DESC_P, value);
            }
        }

        // 17/08/2016: for the visibility of the required skills tab
        private bool _TabSkillsVisible;
        public bool TabSkillsVisible
        {
            get { return _TabSkillsVisible; }
            set { SetProperty(ref _TabSkillsVisible, value); }
        }

        private ObservableCollection<T_GET_COMP_OF_MISSIONWithChecked> _ListOfSkills;
        public ObservableCollection<T_GET_COMP_OF_MISSIONWithChecked> ListOfSkills
        {
            get { return _ListOfSkills; }
            set { SetProperty(ref _ListOfSkills, value); }
        }

        #endregion

        private bool CanExecuteAdd()
        {
            return this.SelectedItem == null;
        }

        private bool CanExecuteUpdate()
        {
            return this.SelectedItem != null;
        }

        private bool CanExecuteDelete()
        {
            // First of all, check if a mission has been selected.
            bool bFlagCanExecuteDelete = this.SelectedItem != null;
            if (bFlagCanExecuteDelete)
            {
                using (var ctx = new BASE_COMPETENCESEntities())
                {
                    // Check if the current mission has skills registered
                    foreach (TCompMission compMission in ctx.TCompMissions)
                    {
                        if (compMission.MISSION_ID==SelectedItem.MISSION_ID)
                        {
                            bFlagCanExecuteDelete = false;
                            break;
                        }
                    }
                }
            }
            return bFlagCanExecuteDelete;
        }

        #region Constructors
        //public WindowMissionViewModel(IEventAggregator eventAggregator)
        public WindowMissionViewModel()
        {
            LoadMissions();

            Func<bool> fbAddMissionCanExecute = delegate
            {
                return this.CanExecuteAdd();
            };

            Func<bool> fbUpdateMissionCanExecute = delegate
            {
                return this.CanExecuteUpdate();
            };

            fbAddMissionCanExecute = CanExecuteAdd;
            fbUpdateMissionCanExecute = CanExecuteUpdate;
            AddMissionCommandDC = new DelegateCommand(this.AddMission, fbAddMissionCanExecute).ObservesProperty(()=> SelectedItem);
            UpdateMissionCommandDC = new DelegateCommand(this.UpdateMission, fbUpdateMissionCanExecute).ObservesProperty(()=>SelectedItem);
            DeleteMissionCommandDC = new DelegateCommand(this.DeleteMission, CanExecuteDelete).ObservesProperty(() => SelectedItem);
            RetCreaModeCommandDC = new DelegateCommand(this.RetCreaMode);
            SaveRequiredSkillsDC = new DelegateCommand(this.SaveRequiredSkills);
            DeleteAllAssociationsDC = new DelegateCommand(this.DeleteAllAssociations);

        }
        #endregion

        #region DelegateCommand
        public DelegateCommand AddMissionCommandDC { get; private set; }
        public DelegateCommand UpdateMissionCommandDC { get; private set; }
        public DelegateCommand DeleteMissionCommandDC { get; private set; }
        public DelegateCommand RetCreaModeCommandDC { get; private set; }
        public DelegateCommand SaveRequiredSkillsDC { get; private set; }
        public DelegateCommand DeleteAllAssociationsDC { get; private set; }
        #endregion

        #region Add or Edit
        private void AddMission()
        {
            using (var ctx = new BASE_COMPETENCESEntities())
            {
                TMission mission = ctx.TMissions.Add(new TMission() { MISSION_LABEL = MISSION_LABEL_P, MISSION_DESC = MISSION_DESC_P });
                ctx.SaveChanges();
                LoadMissions();
                SelectedItem = mission;
            }
        }

        private void UpdateMission()
        {
            using (var ctx = new BASE_COMPETENCESEntities())
            {
                TMission mission = ctx.TMissions.Where(m => m.MISSION_ID == SelectedItem.MISSION_ID).FirstOrDefault();
                mission.MISSION_LABEL = MISSION_LABEL_P;
                mission.MISSION_DESC = MISSION_DESC_P;
                ctx.SaveChanges();
                LoadMissions();
                SelectedItem = mission;
            }
        }

        private void DeleteMission()
        {
            MessageBoxResult res = MessageBox.Show("Voulez-vous supprimer la mission " + SelectedItem.MISSION_LABEL + " ?", "Suppression", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if (res == MessageBoxResult.OK)
            {
                using (var ctx = new BASE_COMPETENCESEntities())
                {
                    TMission mission = ctx.TMissions.Where(m => m.MISSION_ID == SelectedItem.MISSION_ID).FirstOrDefault();
                    ctx.TMissions.Remove(mission);
                    ctx.SaveChanges();
                    LoadMissions();

                }
            }
        }

        private void RetCreaMode()
        {
            SelectedItem = null;
        }

        #endregion

        #region Required skills
        private void SaveRequiredSkills()
        {
            using (var ctx = new BASE_COMPETENCESEntities())
            {
                //ctx.TCompMissions.Load();
                foreach (T_GET_COMP_OF_MISSIONWithChecked gc in ListOfSkills)
                {
                    System.Diagnostics.Debug.Write(gc.COMP_NAME + ", ");
                    System.Diagnostics.Debug.Write(gc.LEVEL_COMP_MIN);
                    System.Diagnostics.Debug.WriteLine(gc.SkillWithMission);
                    TCompMission compMission = ctx.TCompMissions.Where(cm => ((cm.COMP_ID == gc.COMP_ID) && (cm.MISSION_ID == MISSION_ID_P))).FirstOrDefault();
                    // Add a skill
                    if (gc.SkillWithMission)
                    {
                        // If the association skill-mission does not exist
                        if (compMission == null)
                        {
                            TCompMission compmiss = new TCompMission()
                            {
                                COMP_ID = gc.COMP_ID,
                                LEVEL_COMP_MIN = gc.LEVEL_COMP_MIN,
                                MISSION_ID = MISSION_ID_P
                            };
                            ctx.TCompMissions.Add(compmiss);
                            ctx.SaveChanges();
                        }
                        else
                        {
                            compMission.LEVEL_COMP_MIN = gc.LEVEL_COMP_MIN;
                        }
                    }
                    else // Remove a skill
                    {
                        if (compMission != null)
                        {
                            ctx.TCompMissions.Remove(compMission);
                            gc.LEVEL_COMP_MIN = 0;
                            gc.SkillWithMission = false;
                        }
                    }
                    
                }
                ctx.SaveChanges();
            }
        }

        private void DeleteAllAssociations()
        {
            MessageBoxResult res = MessageBox.Show("Voulez-vous supprimer les associations pour la mission " + SelectedItem.MISSION_LABEL + " ?", "Suppression", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if (res == MessageBoxResult.OK)
            {
                using (var ctx = new BASE_COMPETENCESEntities())
                {
                    IEnumerable<TCompMission> entities = ctx.TCompMissions.Where(cm => cm.MISSION_ID == MISSION_ID_P);
                    ctx.TCompMissions.RemoveRange(entities);
                    ctx.SaveChanges(); // à remettre

                    // Either for solution 2
                    //////foreach (T_GET_COMP_OF_MISSIONWithChecked gc in ListOfSkills)
                    //////{
                    //////    gc.LEVEL_COMP_MIN = 0;
                    //////    gc.SkillWithMission = false;
                    //////}

                    // Or for solution 2
                    for (int i = 0; i < ListOfSkills.Count; i++)
                    {
                        ListOfSkills[i].SkillWithMission = false;
                        ListOfSkills[i].LEVEL_COMP_MIN = 0;
                    }

                    /* Solution 1 OK, but better with solution 2 (INotifyPropertyChanged in the class T_GET_COMP_OF_MISSIONWithChecked)
                    ListOfSkills = new ObservableCollection<T_GET_COMP_OF_MISSIONWithChecked>();

                    LoadSkillsForMission(ctx);
                    */
                }
            }
        }

        #endregion

        private void LoadMissions()
        {
            using (var ctx = new BASE_COMPETENCESEntities())
            {
                ctx.TMissions.Load();                
                ListMission = ctx.TMissions.Local;
            }
        }

        private void LoadSkillsForMission(BASE_COMPETENCESEntities ctx)
        {
            ctx.TComps.Load();
            foreach (TComp compe in ctx.TComps.Local)
            {
                T_GET_COMP_OF_MISSIONWithChecked getCompOfMission = new T_GET_COMP_OF_MISSIONWithChecked();
                getCompOfMission.COMP_ID = compe.COMP_ID;
                getCompOfMission.COMP_NAME = compe.COMP_NAME;
                // Case à cocher ou non
                List<sp_GET_COMP_OF_MISSION_Result> lst_gcm = ctx.sp_GET_COMP_OF_MISSION(MISSION_ID_P).ToList();
                sp_GET_COMP_OF_MISSION_Result sp = lst_gcm.FirstOrDefault(cm => cm.COMP_ID == compe.COMP_ID);
                getCompOfMission.SkillWithMission = lst_gcm.FirstOrDefault(cm => cm.COMP_ID == compe.COMP_ID) != null;
                if (sp != null)
                    getCompOfMission.LEVEL_COMP_MIN = sp.LEVEL_COMP_MIN;
                ListOfSkills.Add(getCompOfMission);
            }
        }
    }
}
