﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Collections.ObjectModel;
using DAL;
using System.Windows.Input;
using Skills_Management.ViewModels;
using System.Windows;
using Skills_Management.Views;
using Prism.Interactivity.InteractionRequest;
// 18/07/2016 : en commentaire, au profit de la ligne suivante, et ceci pour avoir la méthode ObservesProperty
// pour la classe DelegateCommand.
//using Microsoft.Practices.Prism.Commands;
using Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Prism.Events;

using Skills_Management.Notifications;
using GlobalResources;
using Events;
using Microsoft.Practices.Unity;
using Prism.Modularity;
using Microsoft.Practices.ServiceLocation;

namespace Skills_Management.ViewModels
{
    public class WindowFamCompViewModel:BindableBase
    {
        private bool fbCanExecuteUpdate()
        {
            return this.SelectedItem != null;
        }

        // 11/07/2016 : CanExecute for Delete
        private bool fbCanExecuteDelete()
        {
            bool bFlagDeleteEnabled = fbCanExecuteUpdate();
            if (bFlagDeleteEnabled)
            {
                using (var ctx = new BASE_COMPETENCESEntities())
                {
                    int intNumSkillsAtt = 0;
                    // Check whether skills have been attached to the skills family
                    foreach (TComp comp in ctx.TComps)
                    {
                        if (comp.FK_FAM_COMP_ID == SelectedItem.FAM_COMP_ID)
                            intNumSkillsAtt++;
                    }
                    bFlagDeleteEnabled = (intNumSkillsAtt == 0);
                    NumSkillsAttached = intNumSkillsAtt.ToString() + " compétence(s) rattachée(s) à cette famille de compétences.";
                }
            }
            return bFlagDeleteEnabled;
        }

        public WindowFamCompViewModel()
        {
            #region Events
            ClassGlob.Eventaggr.GetEvent<WindowFamCompEvent>().Subscribe(LoadFamComp);
            #endregion

            #region Interaction
            #region Test
            // The InteractionRequest class requires a parameter that has to inherit from the INotification class.
            this.NotificationRequest = new InteractionRequest<INotification>();
            // Commands for each of the buttons. Each of these raise a different interaction request.
            this.RaiseNotificationCommand = new DelegateCommand(this.RaiseNotification);
            #endregion

            #region Add, Update and Delete Fam Comp
            // 07/07/2016
            this.AddUpdateFamCompRequest = new InteractionRequest<IConfirmation>();
            this.AddFamCompCommand = new DelegateCommand(this.AddFamCompConfirmation);

            // 08/07/2016 : Update Famille de compétences
            // 18/07/2016 : les lignes ci-dessous, pour Update et Delete Fam Comp, sont modifiées avec l'ajout de ObservesProperty.
            //this.UpdateFamCompCommand = new DelegateCommand(this.UpdateFamCompConfirmation, fbCanExecuteUpdate);
            this.UpdateFamCompCommand = new DelegateCommand(this.UpdateFamCompConfirmation, fbCanExecuteUpdate).ObservesProperty(()=> SelectedItem);
            // Delete Famille de compétences
            this.DelConfirmationRequest = new InteractionRequest<IConfirmation>();
            //this.DeleteFamCompCommand = new DelegateCommand(this.DelFamCompConfirmation, fbCanExecuteDelete);
            this.DeleteFamCompCommand = new DelegateCommand(this.DelFamCompConfirmation, fbCanExecuteDelete).ObservesProperty(() => SelectedItem);

            #endregion
            #endregion
        }

        #region For Events
        private void LoadFamComp(bool bAddItem = false)
        {
            using (var ctx = new BASE_COMPETENCESEntities())
            {
                ctx.TFamComps.Load();
                ListFamComp = ctx.TFamComps.Local;
                if (bAddItem)
                    ListFamComp.Insert(0, new TFamComp());
            }
        }

        #endregion

        #region Properties

        private ObservableCollection<TFamComp> _ListFamComp;
        public ObservableCollection<TFamComp> ListFamComp
        {
            get
            {
                return _ListFamComp;
            }
            set
            {
                _ListFamComp = value;
                this.OnPropertyChanged(()=>this.ListFamComp);
            }
        }

        private TFamComp _SelectedItem;
        public TFamComp SelectedItem
        {
            get
            {
                return _SelectedItem;
            }
            set
            {
                _SelectedItem = value;
                this.OnPropertyChanged(() => this.SelectedItem);
                // 18/07/2016 : deux lignes ci-dessous inutiles
                //UpdateFamCompCommand.RaiseCanExecuteChanged();
                //DeleteFamCompCommand.RaiseCanExecuteChanged();
            }
        }

        //11/07/2016 : Number of skills attached to this family
        private string _NumSkillsAttached;
        public string NumSkillsAttached
        {
            get
            {
                return _NumSkillsAttached;
            }
            set
            {
                _NumSkillsAttached = value;
                OnPropertyChanged(() => NumSkillsAttached);
            }
        }

        #endregion

        #region Interaction
        #region Test Notification
        private string resultMessage;

        public InteractionRequest<INotification> NotificationRequest { get; private set; }

        public ICommand RaiseNotificationCommand { get; private set; }

        public string InteractionResultMessage
        {
            get
            {
                return this.resultMessage;
            }
            set
            {
                this.resultMessage = value;
                this.OnPropertyChanged("InteractionResultMessage");
            }
        }

        private void RaiseNotification()
        {
            // By invoking the Raise method we are raising the Raised event and triggering any InteractionRequestTrigger that
            // is subscribed to it.
            // As parameters we are passing a Notification, which is a default implementation of INotification provided by Prism
            // and a callback that is executed when the interaction finishes.
            this.NotificationRequest.Raise(
               new Notification { Content = "Notification Message", Title = "Notification" },
               n => { InteractionResultMessage = "The user was notified."; });
        }

        #endregion

        #region CommonAddUpdateConfirmation
        private void AddUpdateFamCompConfirmation(string p_CreaOrMod, TFamComp pFamComp = null)
        {
            // The custom popup view in this case has its own view model which implements the IInteractionRequestAware interface
            // therefore, its Notification property will be automatically populated with this notification by the PopupWindowAction.
            // Like this that view model is able to recieve data from this one without knowing each other.

            // 13/07/2016 : ligne ServiceLocator.Current.GetInstance...
            // trouvée à la page http://www.infragistics.com/community/blogs/blagunas/archive/2013/08/06/prism-dynamically-discover-and-load-modules-at-runtime.aspx
            IModuleManager manager = ServiceLocator.Current.GetInstance<IModuleManager>();
            Type CUFamCompModuleType = typeof(FamCompModule.CR_UPFamComp.CUFamCompModule);
            manager.LoadModule(CUFamCompModuleType.Name);

            CUFamCompNotification notification = new CUFamCompNotification();
            notification.Title = p_CreaOrMod + " de famille de compétences";
            notification.famComp = pFamComp;
            this.AddUpdateFamCompRequest.Raise(notification,
                returned =>
                {
                    this.LoadFamComp();
                });
        }
        #endregion

        // 07/07/2016
        #region Add Skills Family
        public ICommand AddFamCompCommand { get; private set; }
        public InteractionRequest<IConfirmation> AddUpdateFamCompRequest { get; private set; }

        private void AddFamCompConfirmation()
        {
            AddUpdateFamCompConfirmation("Création");
        }

        #endregion

        #region Update Skills Family
        public DelegateCommand UpdateFamCompCommand { get; private set; }
        public InteractionRequest<IConfirmation> UpdateFamCompRequest { get; private set; }

        private void UpdateFamCompConfirmation()
        {
            AddUpdateFamCompConfirmation("Modification", this.SelectedItem);
        }
        #endregion

        #region Delete Skills Family
        public DelegateCommand DeleteFamCompCommand { get; private set; }
        public InteractionRequest<IConfirmation> DelConfirmationRequest { get; private set; }

        private void DelFamCompConfirmation()
        {
            // By invoking the Raise method we are raising the Raised event and triggering any InteractionRequestTrigger that
            // is subscribed to it.
            // As parameters we are passing a Confirmation, which is a default implementation of IConfirmation (which inherits
            // from INotification) provided by Prism and a callback that is executed when the interaction finishes.
            this.DelConfirmationRequest.Raise(
                new Confirmation { Content = "Voulez-vous supprimer la famille de compétences " + this.SelectedItem.FAM_COMP_NAME + "?", Title = "Suppression" },
                c =>
                {
                    if (c.Confirmed)
                    {
                        using (var ctx = new BASE_COMPETENCESEntities())
                        {
                            int IdFamComp = this.SelectedItem.FAM_COMP_ID;

                            TFamComp fcomp = ctx.TFamComps.Where(t => t.FAM_COMP_ID == IdFamComp).FirstOrDefault();
                            ctx.TFamComps.Remove(fcomp);

                            ctx.SaveChanges();

                            this.LoadFamComp();
                            // Inutile ici
                            //LSTFamCompViewModel.Wind_FamComp.RefreshItems();
                        };
                    }
                });
        }
        #endregion

        #endregion
    }
}
