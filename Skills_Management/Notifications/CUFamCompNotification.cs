﻿using DAL;
using Prism.Interactivity.InteractionRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skills_Management.Notifications
{
    class CUFamCompNotification : Confirmation
    {
        public CUFamCompNotification()
        {

        }

        public TFamComp famComp { get; set; }
    }
}
