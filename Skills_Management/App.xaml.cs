﻿using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Skills_Management
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        // 07/07/2016 : variable globale. À voir s'il est possible de l'enlever, sans pour autant supprimer
        //prism:ViewModelLocator.AutoWireViewModel="True" dans chaque View.

        //public static IEventAggregator Eventaggr = new EventAggregator();
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // Code vu à la page https://github.com/brianlagunas/PrismViewModelLocator/blob/master/ViewModelLocatorDemo/App.xaml.cs
            IUnityContainer _container = new UnityContainer();

            ViewModelLocationProvider.SetDefaultViewModelFactory((type) =>
            {
                return _container.Resolve(type);
            });

            // 07/07/2016 : inutile en fait
            //_container.RegisterType<IEventAggregator, EventAggregator>();

            var bs = new Bootstrapper();

            bs.Run();


        }
    }
}
