﻿using Skills_Management.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Prism.Unity;

using Prism.Modularity;
using Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using System.Windows.Controls;
using Infrastruct;
using CompModule.Views;
using CollaModule.Colla;
using CollaModule.Comp_Colla;
//using CompModule.Views;


namespace Skills_Management
{
    public class Bootstrapper : UnityBootstrapper
    {
        protected override DependencyObject CreateShell()
        {
            //return new WindowFamComp();
            //return new WindowComp();
            //return new WindowCompColla();
            //return new WindowListCollaWithSkills();
            return new WindowMission();
        }

        protected override void InitializeShell()
        {
            base.InitializeShell();

            Application.Current.MainWindow = (Window)this.Shell;
            App.Current.MainWindow.Show();
        }

        protected override void ConfigureModuleCatalog()
        {
            base.ConfigureModuleCatalog();

            ModuleCatalog moduleCatalog = (ModuleCatalog)this.ModuleCatalog;
            moduleCatalog.AddModule(typeof(FamCompModule.FamCompModule));
            //13/07/2016 : Loading the module On Demand
            //moduleCatalog.AddModule(typeof(FamCompModule.CR_UPFamComp.CUFamCompModule));
            Type CUFamCompModuleType = typeof(FamCompModule.CR_UPFamComp.CUFamCompModule);
            this.ModuleCatalog.AddModule(new ModuleInfo()
            {
                ModuleName = CUFamCompModuleType.Name,
                ModuleType = CUFamCompModuleType.AssemblyQualifiedName,
                InitializationMode = InitializationMode.OnDemand
            });
            // Module Comp
            //moduleCatalog.AddModule(typeof(CompModule.CompModule));
            // Lignes pour On Demand. Sinon, mettre en commentaire, et modifier dans la classe WindowCompViewModel.
            // Et retirer le commentaire à la ligne ci-dessus.
            CUFamCompModuleType = typeof(CompModule.CompModule);
            this.ModuleCatalog.AddModule(new ModuleInfo()
            {
                ModuleName = CUFamCompModuleType.Name,
                ModuleType = CUFamCompModuleType.AssemblyQualifiedName,
                InitializationMode = InitializationMode.OnDemand
            });

            // 25/07/2016 : Module TableCollaModule
            Type TableCollaModuleType = typeof(TableCollaModule);
            //moduleCatalog.AddModule(TableCollaModuleType);
            this.ModuleCatalog.AddModule(new ModuleInfo()
            {
                ModuleName = TableCollaModuleType.Name,
                ModuleType = TableCollaModuleType.AssemblyQualifiedName,
                InitializationMode = InitializationMode.OnDemand
            });

            //Module TableCollaModule
            Type TableComp_CollaModuleType = typeof(TableComp_CollaModule);
            //moduleCatalog.AddModule(TableCollaModuleType);
            this.ModuleCatalog.AddModule(new ModuleInfo()
            {
                ModuleName = TableComp_CollaModuleType.Name,
                ModuleType = TableComp_CollaModuleType.AssemblyQualifiedName,
                InitializationMode = InitializationMode.OnDemand
            });
        }

        protected override RegionAdapterMappings ConfigureRegionAdapterMappings()
        {
            //return base.ConfigureRegionAdapterMappings();

            //RegionAdapterMappings regionAdapterMappings = ServiceLocator.Current.GetInstance<RegionAdapterMappings>();
            var regionAdapterMappings = base.ConfigureRegionAdapterMappings();
            if (regionAdapterMappings != null)
            {
                regionAdapterMappings.RegisterMapping(typeof(Grid), ServiceLocator.Current.GetInstance<GridRegionAdapter>());
                regionAdapterMappings.RegisterMapping(typeof(StackPanel), ServiceLocator.Current.GetInstance<StackPanelRegionAdapter>());
                
            }

            return regionAdapterMappings;
        }

        //18/07/2016
        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();

            // En fait, les deux lignes ci-dessous dont dans la méthode Initialize de la classe CompModule.
            //Container.RegisterTypeForNavigation<UCButtonCompAdd>("UCButtonCompAdd");
            //Container.RegisterTypeForNavigation<UCButtonCompUDRMC>("UCButtonCompUDRMC");
        }

    }
}
