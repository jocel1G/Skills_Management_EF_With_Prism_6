﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class T_GET_COMP_OF_MISSIONWithChecked:sp_GET_COMP_OF_MISSION_Result, INotifyPropertyChanged
    {
        private bool _SkillWithMission;
        public bool SkillWithMission
        {
            get
            {
                return _SkillWithMission;
            }
            set
            {
                if (_SkillWithMission!=value)
                {
                    _SkillWithMission = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private byte _LEVEL_COMP_MIN;
        public new byte LEVEL_COMP_MIN
        {
            get
            {
                return _LEVEL_COMP_MIN;
            }
            set
            {
                if (_LEVEL_COMP_MIN != value)
                {
                    _LEVEL_COMP_MIN = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
