﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class TCompWithBoolAndLevel : TComp
    {
        
        public bool COMP_Checked { get; set; }
    
        public byte Level_Collab { get; set; }        
    }
}
