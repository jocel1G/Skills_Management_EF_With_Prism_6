﻿using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalResources.TCompCollaNS
{
    public class ItemsSourceLevel : BindableBase
    {
        private ObservableCollection<byte> _ListOfLevels = new ObservableCollection<byte>();
        public ObservableCollection<byte> ListOfLevels
        {
            get { return _ListOfLevels; }
            set { SetProperty(ref _ListOfLevels, value); }
        }

        private ObservableCollection<byte> _ListOfLevelsWithZero = new ObservableCollection<byte>();
        public ObservableCollection<byte> ListOfLevelsWithZero
        {
            get { return _ListOfLevelsWithZero; }
            set { SetProperty(ref _ListOfLevelsWithZero, value); }
        }

        public ItemsSourceLevel(bool bFlagAddNull = false):this()
        {
            if (bFlagAddNull)
                _ListOfLevels.Insert(0, 0);
        }

        public ItemsSourceLevel()
        {
            for (byte i = 1; i < 20; i++)
                _ListOfLevels.Add(i);
            _ListOfLevelsWithZero.AddRange(ListOfLevels);
            _ListOfLevelsWithZero.Insert(0, 0);
        }
    }
}
