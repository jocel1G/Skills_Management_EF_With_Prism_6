﻿using DAL;
using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalResources.TCompNS
{
    public class ItemsSourceComp : BindableBase
    {
        private ObservableCollection<TComp> _ListComp = new ObservableCollection<TComp>();
        public ObservableCollection<TComp> ListComp
        {
            get { return _ListComp; }
            set { SetProperty(ref _ListComp, value); }
        }

        public ItemsSourceComp()
        {
            using (var ctx = new BASE_COMPETENCESEntities())
            {
                ctx.TComps.Load();
                ListComp = ctx.TComps.Local;
            }
        }
    }
}
