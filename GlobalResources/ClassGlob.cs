﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalResources
{
    public static class ClassGlob
    {
        #region IEventAggregator
        public static IEventAggregator Eventaggr = new EventAggregator();
        #endregion

        #region RegisterTypeForNavigation
        public const string RTFN_UCButtonCompAdd = "UCButtonCompAdd";
        public const string RTFN_UCButtonCompUDRMC = "UCButtonCompUDRMC";
        #endregion
    }
}
