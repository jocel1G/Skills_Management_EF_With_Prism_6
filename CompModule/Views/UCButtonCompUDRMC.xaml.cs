﻿using GlobalResources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CompModule.Views
{
    /// <summary>
    /// Interaction logic for UCButtonComp.xaml
    /// </summary>
    public partial class UCButtonCompUDRMC : UserControl
    {
        public UCButtonCompUDRMC()
        {
            InitializeComponent();
        }

        public string ViewTitle { get { return ClassGlob.RTFN_UCButtonCompUDRMC; } }
    }
}
