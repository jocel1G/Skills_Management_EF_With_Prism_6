﻿using CompModule.Views;
using GlobalResources;
using Infrastruct;
using Microsoft.Practices.Unity;
using Prism.Modularity;
using Prism.Regions;
using Prism.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompModule
{
    public class CompModule: IModule
    {
        private readonly IRegionManager regionManager;

        //19/07/2016
        private readonly IUnityContainer container;

        //public CompModule(IRegionManager regionManager)
        //{
        //    this.regionManager = regionManager;

        //    IUnityContainer _container = new UnityContainer();
        //    this.container = _container;
        //}

        //19/07/2016
        // Cette solution a été trouvée à la page http://stackoverflow.com/questions/3131297/prism-and-wpf-how-to-add-a-module-on-demand
        // Avec le constructeur ci-dessus, en cliquant sur le bouton Retour en mode création, nous avons System.Object à la place du bouton Add.
        public CompModule(IUnityContainer pcontainer, IRegionManager regionManager)
        {
            if (pcontainer == null)
                throw new ArgumentNullException("pcontainer");

            this.container = pcontainer;

            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            
            regionManager.RegisterViewWithRegion(RegionNames.ErrorCompRegion, typeof(Views.UCErrorComp));

            regionManager.RegisterViewWithRegion(RegionNames.ButtonCompRegion, typeof(Views.UCButtonCompUDRMC));            

            container.RegisterTypeForNavigation<UCButtonCompAdd>(ClassGlob.RTFN_UCButtonCompAdd);
            container.RegisterTypeForNavigation<UCButtonCompUDRMC>(ClassGlob.RTFN_UCButtonCompUDRMC);
        }
    }
}
