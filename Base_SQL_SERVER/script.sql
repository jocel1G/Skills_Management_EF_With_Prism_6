USE [master]
GO
/****** Object:  Database [BASE_COMPETENCES]    Script Date: 10/10/2016 11:24:09 ******/
CREATE DATABASE [BASE_COMPETENCES]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BASE_COMPETENCES', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\BASE_COMPETENCES.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BASE_COMPETENCES_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\BASE_COMPETENCES_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BASE_COMPETENCES] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BASE_COMPETENCES].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BASE_COMPETENCES] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BASE_COMPETENCES] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BASE_COMPETENCES] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BASE_COMPETENCES] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BASE_COMPETENCES] SET ARITHABORT OFF 
GO
ALTER DATABASE [BASE_COMPETENCES] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BASE_COMPETENCES] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BASE_COMPETENCES] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BASE_COMPETENCES] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BASE_COMPETENCES] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BASE_COMPETENCES] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BASE_COMPETENCES] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BASE_COMPETENCES] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BASE_COMPETENCES] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BASE_COMPETENCES] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BASE_COMPETENCES] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BASE_COMPETENCES] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BASE_COMPETENCES] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BASE_COMPETENCES] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BASE_COMPETENCES] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BASE_COMPETENCES] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BASE_COMPETENCES] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BASE_COMPETENCES] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [BASE_COMPETENCES] SET  MULTI_USER 
GO
ALTER DATABASE [BASE_COMPETENCES] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BASE_COMPETENCES] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BASE_COMPETENCES] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BASE_COMPETENCES] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [BASE_COMPETENCES] SET DELAYED_DURABILITY = DISABLED 
GO
USE [BASE_COMPETENCES]
GO
/****** Object:  Table [dbo].[TColla]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TColla](
	[COLLA_ID] [int] IDENTITY(1,1) NOT NULL,
	[COLLA_NAME] [varchar](50) NOT NULL,
	[COLLA_FIRST_NAME] [varchar](50) NOT NULL,
	[COLLA_DATE_NAISS] [datetime] NULL,
	[COLLA_MATRICULE] [varchar](20) NULL,
 CONSTRAINT [PK_TColla] PRIMARY KEY CLUSTERED 
(
	[COLLA_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TComp]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TComp](
	[COMP_ID] [int] IDENTITY(1,1) NOT NULL,
	[COMP_NAME] [varchar](60) NOT NULL,
	[FK_FAM_COMP_ID] [int] NOT NULL,
 CONSTRAINT [PK_TComp] PRIMARY KEY CLUSTERED 
(
	[COMP_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TCompColla]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TCompColla](
	[FK_COLLA_ID] [int] NOT NULL,
	[FK_COMP_ID] [int] NOT NULL,
	[LEVEL_COLLA] [tinyint] NULL,
 CONSTRAINT [PK_TCompColla] PRIMARY KEY CLUSTERED 
(
	[FK_COLLA_ID] ASC,
	[FK_COMP_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TCompMission]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TCompMission](
	[MISSION_ID] [int] NOT NULL,
	[COMP_ID] [int] NOT NULL,
	[LEVEL_COMP_MIN] [tinyint] NOT NULL,
 CONSTRAINT [PK_TCompMission] PRIMARY KEY CLUSTERED 
(
	[MISSION_ID] ASC,
	[COMP_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TFamComp]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TFamComp](
	[FAM_COMP_ID] [int] IDENTITY(1,1) NOT NULL,
	[FAM_COMP_NAME] [varchar](40) NOT NULL,
 CONSTRAINT [PK_T_FAM_COMP] PRIMARY KEY CLUSTERED 
(
	[FAM_COMP_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TMission]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TMission](
	[MISSION_ID] [int] IDENTITY(1,1) NOT NULL,
	[MISSION_LABEL] [varchar](50) NOT NULL,
	[MISSION_DESC] [varchar](max) NULL,
 CONSTRAINT [PK_TMission] PRIMARY KEY CLUSTERED 
(
	[MISSION_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[TCompMission] ADD  CONSTRAINT [DF_TCompMission_LEVEL_COMP_MIN]  DEFAULT ((0)) FOR [LEVEL_COMP_MIN]
GO
ALTER TABLE [dbo].[TComp]  WITH CHECK ADD  CONSTRAINT [FK_TComp_TFamComp] FOREIGN KEY([FK_FAM_COMP_ID])
REFERENCES [dbo].[TFamComp] ([FAM_COMP_ID])
GO
ALTER TABLE [dbo].[TComp] CHECK CONSTRAINT [FK_TComp_TFamComp]
GO
ALTER TABLE [dbo].[TCompColla]  WITH CHECK ADD  CONSTRAINT [FK_TCompColla_TColla] FOREIGN KEY([FK_COLLA_ID])
REFERENCES [dbo].[TColla] ([COLLA_ID])
GO
ALTER TABLE [dbo].[TCompColla] CHECK CONSTRAINT [FK_TCompColla_TColla]
GO
ALTER TABLE [dbo].[TCompColla]  WITH CHECK ADD  CONSTRAINT [FK_TCompColla_TComp] FOREIGN KEY([FK_COMP_ID])
REFERENCES [dbo].[TComp] ([COMP_ID])
GO
ALTER TABLE [dbo].[TCompColla] CHECK CONSTRAINT [FK_TCompColla_TComp]
GO
ALTER TABLE [dbo].[TCompMission]  WITH CHECK ADD  CONSTRAINT [FK_TCompMission_TComp] FOREIGN KEY([COMP_ID])
REFERENCES [dbo].[TComp] ([COMP_ID])
GO
ALTER TABLE [dbo].[TCompMission] CHECK CONSTRAINT [FK_TCompMission_TComp]
GO
ALTER TABLE [dbo].[TCompMission]  WITH CHECK ADD  CONSTRAINT [FK_TCompMission_TMission] FOREIGN KEY([MISSION_ID])
REFERENCES [dbo].[TMission] ([MISSION_ID])
GO
ALTER TABLE [dbo].[TCompMission] CHECK CONSTRAINT [FK_TCompMission_TMission]
GO
/****** Object:  StoredProcedure [dbo].[sp_DELETE_COLLA]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DELETE_COLLA] 
	-- Add the parameters for the stored procedure here
	@COLLA_ID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM dbo.TColla
	WHERE COLLA_ID = @COLLA_ID;

	RETURN @@ROWCOUNT;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DELETE_COMP_COLLA]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DELETE_COMP_COLLA] 
	-- Add the parameters for the stored procedure here
	@FK_COLLA_ID INT,
	@FK_COMP_ID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM dbo.TCompColla
	WHERE FK_COLLA_ID=@FK_COLLA_ID
	AND FK_COMP_ID=@FK_COMP_ID;

	RETURN @@ROWCOUNT;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DELETE_COMP_OF_MISSION]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DELETE_COMP_OF_MISSION] 
	-- Add the parameters for the stored procedure here
	@MISSION_ID INT,
	@COMP_ID INT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM TCompMission
	WHERE TCompMission.MISSION_ID=@MISSION_ID
	AND ((TCompMission.COMP_ID=@COMP_ID) OR (@COMP_ID IS NULL));

	RETURN @@ROWCOUNT;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Delete_FAM_COMP]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Delete_FAM_COMP] 
	-- Add the parameters for the stored procedure here
	@FAM_COMP_ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM TFamCOMP
	WHERE FAM_COMP_ID = @FAM_COMP_ID;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DELETE_MISSION]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DELETE_MISSION] 
	-- Add the parameters for the stored procedure here
	@MISSION_ID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE TMission
	WHERE MISSION_ID=@MISSION_ID;

	RETURN @@ROWCOUNT;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteComp]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteComp] 
	-- Add the parameters for the stored procedure here
	@compId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM dbo.TComp
	WHERE COMP_ID = @compId;

	RETURN @@ROWCOUNT;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GET_COLLA]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GET_COLLA] 
	-- Add the parameters for the stored procedure here
	@COLLA_ID int = null,
	@COLLA_NAME VARCHAR(50)=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT COLLA_ID, COLLA_NAME, COLLA_FIRST_NAME, COLLA_DATE_NAISS, COLLA_MATRICULE
	FROM dbo.TColla
	WHERE ((COLLA_ID=@COLLA_ID) OR (@COLLA_ID IS NULL))
	AND ((COLLA_NAME=@COLLA_NAME) OR (@COLLA_NAME IS NULL));

	RETURN @@ROWCOUNT;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_GET_COLLA_WITH_COMP]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GET_COLLA_WITH_COMP] 
	-- Add the parameters for the stored procedure here
	@FK_COLLA_ID INT=null,
	@FK_COMP_ID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	dbo.TColla.COLLA_ID, dbo.TColla.COLLA_NAME, dbo.TColla.COLLA_FIRST_NAME,			
			dbo.TCompColla.LEVEL_COLLA
	FROM dbo.TComp
	INNER JOIN dbo.TCompColla ON (dbo.TCompColla.FK_COMP_ID = dbo.TComp.COMP_ID)
	INNER JOIN dbo.TColla ON (dbo.TCompColla.FK_COLLA_ID=dbo.TColla.COLLA_ID)
	WHERE ((dbo.TCompColla.FK_COLLA_ID = @FK_COLLA_ID) OR (@FK_COLLA_ID IS NULL))
	AND (dbo.TCompColla.FK_COMP_ID = @FK_COMP_ID);

	RETURN @@ROWCOUNT;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GET_COMP_OF_COLLA]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GET_COMP_OF_COLLA] 
	-- Add the parameters for the stored procedure here
	@FK_COLLA_ID int,
	@FK_COMP_ID INT=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	dbo.TComp.COMP_ID, dbo.TComp.COMP_NAME,
			dbo.TFamComp.FAM_COMP_ID, dbo.TFamComp.FAM_COMP_NAME,
			dbo.TCompColla.LEVEL_COLLA, dbo.TCompColla.FK_COMP_ID
	FROM dbo.TComp
	INNER JOIN dbo.TCompColla ON (dbo.TCompColla.FK_COMP_ID = dbo.TComp.COMP_ID)
	INNER JOIN dbo.TFamComp ON (dbo.TComp.FK_FAM_COMP_ID = dbo.TFamComp.FAM_COMP_ID)
	WHERE (dbo.TCompColla.FK_COLLA_ID = @FK_COLLA_ID)
	AND ((dbo.TCompColla.FK_COMP_ID = @FK_COMP_ID) OR (@FK_COMP_ID IS NULL));

	RETURN @@ROWCOUNT;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GET_COMP_OF_MISSION]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GET_COMP_OF_MISSION] 
	-- Add the parameters for the stored procedure here
	@MISSION_ID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TComp.COMP_ID, COMP_NAME, LEVEL_COMP_MIN
	FROM TComp
	INNER JOIN TCompMission ON (TComp.COMP_ID=TCompMission.COMP_ID)
	WHERE TCompMission.MISSION_ID=@MISSION_ID;

	RETURN @@ROWCOUNT;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Get_FAM_COMP]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Get_FAM_COMP]
	-- Add the parameters for the stored procedure here
	@FAM_COMP_ID INT = null,
	@FAM_COMP_NAME VARCHAR(40) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TFamComp.FAM_COMP_ID, TFamComp.FAM_COMP_NAME
	from TFamComp
	where ((TFamComp.FAM_COMP_ID = @FAM_COMP_ID) OR (@FAM_COMP_ID IS NULL));

	RETURN @@ROWCOUNT;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GET_MISSION]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GET_MISSION]
	-- Add the parameters for the stored procedure here
	@MISSION_ID int=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT MISSION_ID, MISSION_LABEL, MISSION_DESC
	FROM TMission
	WHERE ((MISSION_ID=@MISSION_ID) OR (@MISSION_ID IS NULL));

	RETURN @@ROWCOUNT;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetComp]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetComp] 
	-- Add the parameters for the stored procedure here
	@compId int = null,
	@compName varchar(60) = null,
	@fkFamCompId int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT COMP_ID, COMP_NAME, FK_FAM_COMP_ID
	FROM dbo.TComp
	WHERE ((COMP_ID=@compId) OR (@compId IS NULL))
	AND ((COMP_NAME=@compName) OR (@compName IS NULL))
	AND ((FK_FAM_COMP_ID=@fkFamCompId) OR (@fkFamCompId IS NULL));

	RETURN @@ROWCOUNT;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_INSERT_COLLA]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_INSERT_COLLA]
	-- Add the parameters for the stored procedure here
	@COLLA_NAME VARCHAR(50),
	@COLLA_FIRST_NAME VARCHAR(50),
	@COLLA_DATE_NAISS DATETIME=null,
	@COLLA_MATRICULE VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.TColla
	(COLLA_NAME, COLLA_FIRST_NAME, COLLA_DATE_NAISS, COLLA_MATRICULE)
	VALUES(@COLLA_NAME, @COLLA_FIRST_NAME, @COLLA_DATE_NAISS, @COLLA_MATRICULE);

	SELECT SCOPE_IDENTITY() AS CollaId
END

GO
/****** Object:  StoredProcedure [dbo].[sp_INSERT_COMP_COLLA]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_INSERT_COMP_COLLA] 
	-- Add the parameters for the stored procedure here
	@FK_COLLA_ID INT,
	@FK_COMP_ID INT,
	@LEVEL_COLLA TINYINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.TCompColla
	(FK_COLLA_ID, FK_COMP_ID, LEVEL_COLLA)
	VALUES(@FK_COLLA_ID, @FK_COMP_ID, @LEVEL_COLLA);

	RETURN @@ROWCOUNT;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_INSERT_COMP_OF_MISSION]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_INSERT_COMP_OF_MISSION] 
	-- Add the parameters for the stored procedure here
	@MISSION_ID INT,
	@COMP_ID INT,
	@LEVEL_COMP_MIN TINYINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO TCompMission
	(MISSION_ID, COMP_ID, LEVEL_COMP_MIN)
	VALUES(@MISSION_ID, @COMP_ID, @LEVEL_COMP_MIN);
	

	RETURN @@ROWCOUNT;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Insert_FAM_COMP]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Insert_FAM_COMP] 
	-- Add the parameters for the stored procedure here
	@FAM_COMP_NAME VARCHAR(40)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.TFamComp
	(FAM_COMP_NAME)
	VALUES(@FAM_COMP_NAME);

	SELECT SCOPE_IDENTITY() AS StudentId;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_INSERT_MISSION]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_INSERT_MISSION] 
	-- Add the parameters for the stored procedure here
	@MISSION_LABEL varchar(50),
	@MISSION_DESC varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO TMission
	(MISSION_LABEL, MISSION_DESC)
	VALUES(@MISSION_LABEL, @MISSION_DESC);

	SELECT SCOPE_IDENTITY() AS MissionId;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertComp]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertComp] 
	-- Add the parameters for the stored procedure here
	@compName varchar(60) = null,
	@fkFamCompId int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.TComp
	(COMP_NAME, FK_FAM_COMP_ID)
	VALUES(@compName, @fkFamCompId);

	SELECT SCOPE_IDENTITY() AS CompId;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_UPDATE_COLLA]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UPDATE_COLLA] 
	-- Add the parameters for the stored procedure here
	@COLLA_ID int,
	@COLLA_NAME VARCHAR(50),
	@COLLA_FIRST_NAME VARCHAR(50),
	@COLLA_DATE_NAISS DATETIME=null,
	@COLLA_MATRICULE VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.TColla
	SET COLLA_NAME=@COLLA_NAME,
		COLLA_FIRST_NAME=@COLLA_FIRST_NAME,
		COLLA_DATE_NAISS=@COLLA_DATE_NAISS,
		COLLA_MATRICULE = @COLLA_MATRICULE
	WHERE COLLA_ID=@COLLA_ID;

	RETURN @@ROWCOUNT;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UPDATE_COMP_COLLA]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UPDATE_COMP_COLLA]
	-- Add the parameters for the stored procedure here
	@FK_COLLA_ID INT,
	@FK_COMP_ID INT,
	@LEVEL_COLLA TINYINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.TCompColla
	SET LEVEL_COLLA=@LEVEL_COLLA
	WHERE FK_COLLA_ID=@FK_COLLA_ID
	AND FK_COMP_ID=@FK_COMP_ID;

	RETURN @@ROWCOUNT;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UPDATE_COMP_OF_COLLA]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UPDATE_COMP_OF_COLLA] 
	-- Add the parameters for the stored procedure here
	@MISSION_ID INT,
	@COMP_ID INT,
	@LEVEL_COMP_MIN TINYINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE TCompMission
	SET LEVEL_COMP_MIN=@LEVEL_COMP_MIN
	WHERE MISSION_ID=@MISSION_ID
	AND COMP_ID=@COMP_ID;

	RETURN @@ROWCOUNT;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Update_FAM_COMP]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Update_FAM_COMP] 
	-- Add the parameters for the stored procedure here
	@FAM_COMP_ID int,
	@FAM_COMP_NAME varchar(40)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE TFamComp
	SET FAM_COMP_NAME = @FAM_COMP_NAME
	WHERE FAM_COMP_ID = @FAM_COMP_ID;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UPDATE_MISSION]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UPDATE_MISSION] 
	-- Add the parameters for the stored procedure here
	@MISSION_ID INT,
	@MISSION_LABEL varchar(50),
	@MISSION_DESC varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE TMission
	SET MISSION_LABEL=@MISSION_LABEL,
		MISSION_DESC=@MISSION_DESC
	WHERE MISSION_ID = @MISSION_ID;

	RETURN @@ROWCOUNT;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateComp]    Script Date: 10/10/2016 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateComp] 
	-- Add the parameters for the stored procedure here
	@compId int,
	@compName varchar(60),
	@fkFamCompId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.TComp
	SET COMP_NAME = @compName,
	FK_FAM_COMP_ID = @fkFamCompId
	WHERE COMP_ID = @compId;

	RETURN @@ROWCOUNT;
END

GO
USE [master]
GO
ALTER DATABASE [BASE_COMPETENCES] SET  READ_WRITE 
GO
