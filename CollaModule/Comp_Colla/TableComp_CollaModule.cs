﻿using CollaModule.Comp_Colla.Views;
using Infrastruct;
using Microsoft.Practices.Unity;
using Prism.Modularity;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollaModule.Comp_Colla
{
    public class TableComp_CollaModule : IModule
    {
        private readonly IUnityContainer container;
        private readonly IRegionManager regionManager;

        public TableComp_CollaModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            this.container.Resolve<CompCollaDataService>();

            this.regionManager.RegisterViewWithRegion(RegionNames.TableCompCollaRegion, typeof(UCCompColla));
        }
    }
}
