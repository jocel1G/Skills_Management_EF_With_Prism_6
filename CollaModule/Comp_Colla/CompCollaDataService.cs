﻿using DAL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Data.Entity;

namespace CollaModule.Comp_Colla
{
    public class CompCollaDataService
    {
        // (1)
        //public ICollectionView getCompCollas(int FK_COLLA_ID)
        //{
        //    using (var ctx = new BASE_COMPETENCESEntities())
        //    {                
        //        return new ListCollectionView(ctx.sp_GET_COMP_OF_COLLA(FK_COLLA_ID, null).ToArray());

        //    }
        //}

        // (2)
        public ObservableCollection<sp_GET_COMP_OF_COLLA_Result> getCompCollas(int FK_COLLA_ID)
        {
            using (var ctx = new BASE_COMPETENCESEntities())
            {
                return new ObservableCollection<sp_GET_COMP_OF_COLLA_Result>(ctx.sp_GET_COMP_OF_COLLA(FK_COLLA_ID, null).ToArray());

            }
        }
    }
}
