﻿using CollaModule.Colla.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using DAL;
using System.Windows.Data;
using System.Collections.ObjectModel;
using System.Data.Entity;

namespace CollaModule.Colla
{
    public class CollaDataService : ICollaDataService
    {
        public ICollectionView getCollas()
        {
            using (var ctx = new BASE_COMPETENCESEntities())
            {
                ctx.TCollas.Load();
                ObservableCollection<TColla> obs = ctx.TCollas.Local;
                
                return (new ListCollectionView(obs));
            }
               
        }
        
    }
}
