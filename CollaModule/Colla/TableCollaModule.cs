﻿using CollaModule.Colla.Interfaces;
using CollaModule.Colla.Views;
using Infrastruct;
using Microsoft.Practices.Unity;
using Prism.Modularity;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollaModule.Colla
{
    public class TableCollaModule : IModule
    {
        private readonly IUnityContainer container;
        private readonly IRegionManager regionManager;

        public TableCollaModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            this.container.RegisterType<ICollaDataService, CollaDataService>();

            this.regionManager.RegisterViewWithRegion(RegionNames.GridCollaRegion, typeof(UCGridColla));
        }
    }
}
