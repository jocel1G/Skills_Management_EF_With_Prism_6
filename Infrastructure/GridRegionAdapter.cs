﻿using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Infrastruct
{
    public class GridRegionAdapter : RegionAdapterBase<Grid>
    {
        public GridRegionAdapter(IRegionBehaviorFactory regionBehaviorFactory) : base(regionBehaviorFactory)
        {
        }

        protected override void Adapt(IRegion region, Grid regionTarget)
        {
            //throw new NotImplementedException();
            if (regionTarget == null) throw new ArgumentNullException("regionTarget");
            // À voir comme à la page https://msdn.microsoft.com/en-us/library/gg430866(v=pandp.40).aspx#sec25
            //bool contentIsSet = regionTarget...
            region.Views.CollectionChanged += (s, e) =>
            {
                //Add 
                if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
                    foreach (FrameworkElement element in e.NewItems)
                        regionTarget.Children.Add(element);



                //Removal 
                if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
                    foreach (FrameworkElement element in e.OldItems)
                        if (regionTarget.Children.Contains(element))
                            regionTarget.Children.Remove(element);
            };

        }

        protected override IRegion CreateRegion()
        {
            return new SingleActiveRegion();
        }


    }
}
