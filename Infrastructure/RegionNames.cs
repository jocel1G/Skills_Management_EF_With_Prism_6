﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastruct
{
    public static class RegionNames
    {
        #region Famille de compétences, liste
        public const string ButtonFamCompRegion = "ButtonFamCompRegion";
        public const string DataGridFamCompRegion = "DataGridFamCompRegion";
        #endregion

        #region CU Famille de compétences
        public const string UCFamCompTextBoxLabelRegion = "UCFamCompTextBoxLabelRegion";
        public const string FamCompButtonRegion = "FamCompButtonRegion";
        #endregion

        #region Compétences
        public const string ErrorCompRegion = "ErrorCompRegion";
        public const string DataGridCompRegion = "DataGridCompRegion";

        public const string ButtonCompRegion = "ButtonCompRegion";
        #endregion

        #region Table Colla
        public const string GridCollaRegion = "GridCollaRegion";
        #endregion

        #region Table Compétences des colla
        public const string TableCompCollaRegion = "TableCompCollaRegion";
        public const string ButtonCompCollaRegion = "ButtonCompCollaRegion";
        #endregion

        #region Missions
        public const string TabRegionMission = "TabRegionMission";
        #endregion
    }
}
