﻿using Infrastruct;
using Prism.Modularity;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamCompModule
{
    public class FamCompModule : IModule
    {
        private readonly IRegionManager regionManager;

        public FamCompModule(IRegionManager regionManager)
        {
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            regionManager.RegisterViewWithRegion(RegionNames.ButtonFamCompRegion, typeof(Views.UCbtnFamComp));
            regionManager.RegisterViewWithRegion(RegionNames.DataGridFamCompRegion, typeof(Views.UCDataGridFamCompRegion));            
        }
    }
}
