﻿using Events;
using GlobalResources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FamCompModule.Views
{
    /// <summary>
    /// Interaction logic for UCDataGridFamCompRegion.xaml
    /// </summary>
    public partial class UCDataGridFamCompRegion : UserControl
    {
        public UCDataGridFamCompRegion()
        {
            InitializeComponent();
        }

        private void MyGrid_Loaded(object sender, RoutedEventArgs e)
        {
            ClassGlob.Eventaggr.GetEvent<WindowFamCompEvent>().Publish(false);
        }
    }


    
}
