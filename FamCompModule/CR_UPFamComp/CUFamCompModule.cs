﻿using Prism.Modularity;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamCompModule.CR_UPFamComp
{
    public class CUFamCompModule : IModule
    {
        private readonly IRegionManager regionManager;

        public CUFamCompModule(IRegionManager regionManager)
        {
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            //throw new NotImplementedException();
            regionManager.RegisterViewWithRegion("UCFamCompTextBoxLabelRegion", typeof(CR_UPFamComp.Views.UCFamCompTextBoxLabel));
            regionManager.RegisterViewWithRegion("FamCompButtonRegion", typeof(CR_UPFamComp.Views.UCFamCompButton));

            

        }
    }
}
